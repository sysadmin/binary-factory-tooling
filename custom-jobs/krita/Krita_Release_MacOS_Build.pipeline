// Ask for parameters we will need later on
def buildParameters = input(
	message: 'Which version of Krita is being built?',
	ok: 'Begin Build',
	parameters: [
		string(defaultValue: '', description: '', name: 'Version', trim: true)
	]
)

// Pull the version we've been given out to a separate variable
// We need to do this otherwise Jenkins will throw its toys and claim we are violating a security sandbox which will expose our instance to vulnerabilities
// The Jenkins Security Sandbox is IMO broken and faulty in this regard
def buildVersion = buildParameters ?: ''

// Request a node to be allocated to us
node( "macOSARM" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Now we download the release tarball, unpack it and rename the directory to something more convenient to use everywhere else
			sh """
				curl -L "https://origin.files.kde.org/krita/.release/${buildVersion}/krita-${buildVersion}.tar.gz" -o "krita-${buildVersion}.tar.gz"

				tar -xf "\$WORKSPACE/krita-${buildVersion}.tar.gz"

				mv krita-${buildVersion} krita
			"""

		}

		// Now retrieve the artifacts
		stage('Retrieving Dependencies') {
			// First we grab the artifacted dependencies built last time round
			copyArtifacts filter: 'krita-macos-deps.tar', projectName: 'Krita_Nightly_MacOS_Dependency_Build'

			// Now we unpack them
			// We also make sure our build workspace is ready at the same time
			sh """
				export BUILDROOT=\$HOME/KritaBuild/

				[[ -d \$BUILDROOT ]] || mkdir \$BUILDROOT
				[[ -s \$BUILDROOT/krita ]] || ln -s \$WORKSPACE/krita \$BUILDROOT/krita

				cd \$BUILDROOT
				tar -xf $WORKSPACE/krita-macos-deps.tar
			"""
		}

		// Let's build Krita now that we have everything we need
		stage('Building Krita') {
			// The first parameter to the script is what it should be doing - which is building and installing Krita
			// The workspace it uses was setup in the previous step
			sh """
				export PATH=\$HOME/Craft/Krita/macos-64-clang/bin/:\$HOME/Craft/Krita/macos-64-clang/dev-utils/bin/:\$PATH
				export BUILDROOT=\$HOME/KritaBuild/

				bash krita/packaging/macos/osxbuild.sh --universal buildinstall
			"""
		}

		// Now we can generate the actual DMG!
		stage('Generating Krita DMG') {
			// The scripts handle everything here, so just run them
			sh """
				export PATH=\$HOME/Craft/Krita/macos-64-clang/bin/:\$HOME/Craft/Krita/macos-64-clang/dev-utils/bin/:\$PATH
				export BUILDROOT=\$HOME/KritaBuild/

				bash krita/packaging/macos/osxdeploy.sh -s="K Desktop Environment e.V. (5433B4KXM8)" -notarize-ac="apple-store@kde.org" -asc-provider="5433B4KXM8"
				mv \$BUILDROOT/*.dmg \$WORKSPACE/
			"""
		}

		// Finally we capture the DMG for distribution to users
		stage('Capturing DMGs') {
			// We use Jenkins artifacts for this to save having to setup additional infrastructure
			archiveArtifacts artifacts: '*.dmg', onlyIfSuccessful: true
		}

		// Cleanup!
		stage('Cleanup') {
			// We need to remove the Build Root to ensure it does not contaminate future builds
			sh """
				rm -rf \$HOME/KritaBuild/
			"""

			// Cleanup the workspace too
			deleteDir()
		}
	}
}
}
