// Request a node to be allocated to us
node( "KritaAppimageBuilder" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Krita Code
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'krita/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/graphics/krita.git']]
			]

		}

		// Now retrieve the artifacts
		stage('Retrieving Dependencies') {
			// First we grab the artifacted dependencies built last time round
			copyArtifacts filter: 'krita-appimage-deps.tar', projectName: 'Krita_Nightly_Appimage_Dependency_Build'

			// Now we unpack them
			sh """
				mkdir -p $HOME/appimage-workspace/
				cd $HOME/appimage-workspace/
				tar -xf $WORKSPACE/krita-appimage-deps.tar
			"""
		}

		// Let's build Krita that we have everything we need
		stage('Building Krita') {
			// The first parameter to the script is where the scripts should work - which is our workspace in this case
			// Otherwise we leave everything in the hands of that script
			sh """
				export PATH=$HOME/tools/bin/:$PATH

				krita/packaging/linux/appimage/build-krita.sh $HOME/appimage-workspace/ $WORKSPACE/krita/ Next
			"""
		}

		// Now we can generate the actual Appimages!
		stage('Generating Krita Appimage') {
			// The scripts handle everything here, so just run them
			sh """
				export PATH=$HOME/tools/bin/:$PATH

				krita/packaging/linux/appimage/build-image.sh $HOME/appimage-workspace/ $WORKSPACE/krita/ Next
				mv $HOME/appimage-workspace/*.appimage $WORKSPACE/
			"""

			// Capture the generated appimages for relocation to the signer
			stash includes: '*.appimage', name: 'appimages'
		}
	}
}
}

// Now we transition over to the privileged signer
// Request a node to be allocated to us
node( "AppimageSigner" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
		// Generate the appimage update
		stage('Signing Appimage') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// We'll need the Binary Factory tooling for this....
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'binary-factory-tooling/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/sysadmin/binary-factory-tooling.git']]
			]

			// Restore the previously captured appimages
			unstash 'appimages'

			// Call the script to perform the signing, then generate the deltas
			sh """
				APPIMAGE_PATH="\$(echo krita-*.appimage)"

				binary-factory-tooling/appimages/sign-appimage.sh \$APPIMAGE_PATH

				zsyncmake -u "\${BUILD_URL}/artifact/\$(basename \${APPIMAGE_PATH})" -o \$APPIMAGE_PATH.zsync \$APPIMAGE_PATH
				mv \$APPIMAGE_PATH.zsync Krita-Next-x86_64.appimage.zsync
			"""

			// We use Jenkins artifacts for this to save having to setup additional infrastructure
			archiveArtifacts artifacts: '*.appimage, *.appimage.zsync', onlyIfSuccessful: true
		}
	}
}
}
