// Request a node to be allocated to us
node( "StaticWeb" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First we need to gather all of the tools we will need to perform the build...
		stage('Retrieving Utilities') {
			// Grab the Binary Factory Tooling (for our tools and configuration)
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/sysadmin/binary-factory-tooling.git']]
			]

			// We also need the Repository Metadata too (to be able to clone all of the repositories)
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'repo-metadata/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/sysadmin/repo-metadata.git']]
			]

			// As well as KApiDox to actually build the API Documentation
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'kapidox/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/frameworks/kapidox.git']]
			]
		}

		// With the utilities retrieved, we can start by cloning the sources we are going to generate the API Documentation of
		stage('Cloning Sources') {
			// Clone the sources
			// Once that is done, retrieve the Dependency Diagrams from the CI system
			sh """
				mkdir sources/
				cd sources/
				cat ../bf-tooling/apidocs/repos-to-process | while read line; do

					repositoryRule=\$(echo \$line | cut -d " " -f 1)
					repositoryBranch=\$(echo \$line | cut -d " " -f 2)

					python3 ../repo-metadata/git-helpers/git-kclone "\$repositoryRule" --branch="\$repositoryBranch"

				done
				cd ../
			"""
		}

		// With the sources available to us, we can now build them...
		stage('Building') {
			// Build the API Docs!!
			sh """
				export PATH=/usr/lib64/qt5/bin/:\$WORKSPACE/kapidox/:\$PATH

				mkdir generated-docs/
				cd generated-docs/
				python3 \$WORKSPACE/kapidox/kapidox/kapidox_generate.py --doxdatadir \$WORKSPACE/kapidox/kapidox/data/ --qtdoc-dir \$WORKSPACE/bf-tooling/apidocs/tags/5.15/ --qtdoc-link https://doc.qt.io/qt-5 --qtdoc-flatten-links \$WORKSPACE/sources/
			"""
		}

		stage('Fetching External Docs') {
			sh """
				cd generated-docs/
				wget https://invent.kde.org/frameworks/extra-cmake-modules/-/jobs/artifacts/master/download?job=docs -O ecm-docs.zip
				unzip ecm-docs.zip
				mv ecm-docs ecm
				rm ecm-docs.zip
			"""
		}

		// With the docs built, now we publish them
		stage('Publishing') {
			// Upload to our server...
			sh """
				rsync -Ha --delete -e "ssh -i ~/WebsitePublishing/tyran-publisher.key" generated-docs/ autodeploy@tyran.kde.org:/srv/www/generated/api.kde.org/
			"""
		}

	}
}
}
