#!/usr/bin/python3
import os
import sys
import json
import fnmatch
import argparse
import configparser

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to determine which jobs need to be registered in Jenkins.')
parser.add_argument('--craftmaster-config', type=str, required=True)
arguments = parser.parse_args()

# Grab our craftmaster configuration file and load it up
craftmasterConfig = configparser.ConfigParser()
craftmasterConfig.read( arguments.craftmaster_config )

# The following section names have special meaning to Craftmaster and should be skipped
sectionsToSkip = ['General', 'Variables', 'GeneralSettings', 'BlueprintSettings']

# Map the various craft platform names to our Pipelines
# The LHS is a glob style regex which should match against the Craft Platform Name
# The RHS is the name of the Pipeline we should use for the job
platformToPipelines = {
    'macos-*': 'macos'
}

# Our output will be a list of Dictionaries, containing several keys:
# 1) The Craft platform to be built
# 2) The Pipeline template to use
# 4) The description for the resulting job
jobsGathered = []

# Let's get started!
for section in craftmasterConfig.sections():
    # Is this a section we want to otherwise ignore?
    # If its name ends with BlueprintSettings we should ignore it as well
    if section in sectionsToSkip or section.endswith('BlueprintSettings'):
        continue

    # Make sure we don't use a previous iterations Pipeline template
    pipelineTemplate = None

    # Determine what Pipeline template we should use
    for platformRule in platformToPipelines:
        # Does it match?
        if fnmatch.fnmatch(section, platformRule):
            # Then grab the name of the Pipeline template we are going to use
            pipelineTemplate = platformToPipelines[platformRule]

    # Did we find a usable template?
    if pipelineTemplate is None:
        # Then we can't do anything for this one alas
        continue

    # Create a nice little description for this job
    jobDescription = "Craft Cache build for {0}".format( section )

    # At this point we are good to go!
    jobEntry = {
        'craftPlatform': section,
        'buildPipeline': pipelineTemplate,
    }

    # Make sure we add it to the list
    jobsGathered.append( jobEntry )

# Now output the jobs we've gathered in JSON to disk
# This will subsequently be read in by a Jenkins DSL script and turned into Jenkins Jobs
filePath = os.path.join( os.getcwd(), 'gathered-jobs.json')
with open(filePath, 'w') as jobsFile:
    json.dump( jobsGathered, jobsFile, sort_keys=True, indent=2  )

# All done!
sys.exit(0)
