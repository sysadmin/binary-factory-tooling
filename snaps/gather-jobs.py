#!/usr/bin/python3
import os
import sys
import json
import yaml
import argparse

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to determine which jobs need to be registered in Jenkins.')
parser.add_argument('--projects', type=str, required=True)
arguments = parser.parse_args()

# Grab our list of projects and their configuration
with open(arguments.projects, 'r') as dataFile:
    # Parse the YAML file
    projectsToCreate = yaml.load( dataFile )

# Our output will be a list of Dictionaries, containing several keys:
# 1) The name of the job
# 2) The description for the resulting job
# 3) The repository to be cloned, and the branch to use
# 4) The folder to look in for 'snapcraft.yaml'
jobsGathered = []

# Let's get started!
for project in projectsToCreate.keys():
    # Grab the configuration for the project out to make it easier to access
    projectConfig = projectsToCreate[ project ]

    # Create a nice little description for this job
    jobDescription = "Snap build for {0}".format( project )

    # Determine the repository url
    repositoryUrl = "https://invent.kde.org/{0}".format( projectConfig['repository'] )

    # At this point we are good to go!
    jobEntry = {
        'name': project,
        'description': jobDescription,
        'repository': repositoryUrl,
        'branchToBuild': projectConfig['branch'],
        'snapDefinitionDirectory': projectConfig['snapDefinitionDirectory'],
    }

    # Make sure we add it to the list
    jobsGathered.append( jobEntry )

# Now output the jobs we've gathered in JSON to disk
# This will subsequently be read in by a Jenkins DSL script and turned into Jenkins Jobs
filePath = os.path.join( os.getcwd(), 'gathered-jobs.json')
with open(filePath, 'w') as jobsFile:
    json.dump( jobsGathered, jobsFile, sort_keys=True, indent=2  )

# All done!
sys.exit(0)
